<?php

namespace Composer\ShipshapeDirectoryInstaller;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class ShipshapePlugin implements PluginInterface
{
    public function activate(Composer $composer, IOInterface $io)
    {
        $installer = new ShipshapeInstaller($io, $composer);
        $composer->getInstallationManager()->addInstaller($installer);
    }
}
