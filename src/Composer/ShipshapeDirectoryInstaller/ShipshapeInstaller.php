<?php

namespace Composer\ShipshapeDirectoryInstaller;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;

class ShipshapeInstaller extends LibraryInstaller
{

    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        return $package->getExtra()["install-directory"];
    }

    /**
     * {@inheritdoc}
     */
    public function supports($packageType)
    {
        if (in_array($packageType, array('shipshape-core-bundle'))) {
            return true;
        }

        return false;
    }

}
